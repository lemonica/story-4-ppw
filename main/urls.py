from django.urls import path, include

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.story3, name='home'),
    path('story-1', views.story1, name='story1'),
    path('story-3-exp', views.story3creation, name='story3exp'),
]
