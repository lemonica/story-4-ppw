from django.shortcuts import render


def story1(request):
    return render(request, 'main/story1/story1.html')

def story3(request):
    return render(request, 'main/story3/index.html')

def story3creation(request):
    return render(request, 'main/story3/creation.html')   