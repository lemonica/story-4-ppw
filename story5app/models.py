from django.db import models

class MataKuliah(models.Model):
    nama_Matkul= models.CharField(max_length=100, default=None)
    dosen = models.CharField(max_length=100)
    jumlah_SKS = models.CharField(max_length=10)
    deskripsi = models.CharField(max_length=200)
    semester = models.CharField(max_length=100)
    ruang = models.CharField(max_length=100)

    def __str__(self):
        return "{}.{}".format(self.id, self.nama_Matkul)
