from django import forms
from .models import MataKuliah

class FormMatkul(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = [
            'nama_Matkul', 
            'dosen', 
            'jumlah_SKS', 
            'deskripsi', 
            'semester', 
            'ruang'
        ]
