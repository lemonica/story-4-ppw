from django.urls import path, include

from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.formulir, name='form'),
    path('list-matkul', views.daftar, name='list'),
    path('detail-matkul/<id_matkul>', views.detail, name='detail'),
    path('delete/<hapus_id>', views.hapus, name='delete'),
]