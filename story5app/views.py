from django.shortcuts import render, redirect
from .forms import FormMatkul
from .models import MataKuliah

# Create your views here.

def daftar(request):
    daftar_matkul = MataKuliah.objects.all()
    context = {
        'daftar_matkul': daftar_matkul,
    }

    return render(request, 'story5/list.html', context)

def formulir(request):
    form = FormMatkul(request.POST or None)
    if form.is_valid() and request.method == 'POST':
        form.save()
        return redirect('story5:list')
    
    context = {
        'form': form
        }

    return render(request, 'story5/form.html', context)

def hapus(request, hapus_id):
    MataKuliah.objects.filter(id=hapus_id).delete()
    return redirect('story5:list')

def detail(request, id_matkul):
    nama_matkul = MataKuliah.objects.get(id=id_matkul).nama_Matkul
    dosen = MataKuliah.objects.get(id=id_matkul).dosen
    sks = MataKuliah.objects.get(id=id_matkul).jumlah_SKS
    desc = MataKuliah.objects.get(id=id_matkul).deskripsi
    semester = MataKuliah.objects.get(id=id_matkul).semester
    ruang = MataKuliah.objects.get(id=id_matkul).ruang

    context = {
        'nama_matkul': nama_matkul,
        'dosen': dosen,
        'sks': sks,
        'deskripsi': desc,
        'semester': semester,
        'ruang': ruang
    }

    return render(request, 'story5/detail.html', context)