from django.test import TestCase, Client
from django.urls import resolve
from .models import Kegiatan, Pendaftar
from .views import *
from .urls import *


class Story6TestCase(TestCase):

    def test_views_index(self):
        found = resolve('/story6/')            
        self.assertEqual(found.func, index)

    def test_views_add_kegiatan(self):
        found = resolve('/story6/form-kegiatan/')            
        self.assertEqual(found.func, add_kegiatan)

    def test_views_add_peserta(self):
        Kegiatan.objects.create(nama_kegiatan='SDA')
        found = resolve('/story6/form-pendaftar/1')            
        self.assertEqual(found.func, add_pendaftar)

    def test_url_story6_is_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_url_form_kegiatan_is_exist(self):
        response = Client().get('/story6/form-kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_template_index(self):
        response = Client().get('/story6/')            
        self.assertTemplateUsed(response, 'story6/index.html')

    def test_template_form_kegiatan(self):
        response = Client().get('/story6/form-kegiatan/')            
        self.assertTemplateUsed(response, 'story6/form-kegiatan.html')
    
    def test_model_create_kegiatan(self):
        Kegiatan.objects.create(nama_kegiatan="SDA")
        jumlah = Kegiatan.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_model_create_pendaftar(self):
        Kegiatan.objects.create(nama_kegiatan="SDA")
        obj = Kegiatan.objects.get(id=1)
        Pendaftar.objects.create(kegiatan_pendaftar=obj, nama_pendaftar="Izuri")
        jumlah = Pendaftar.objects.all().count()
        self.assertEqual(jumlah, 1)





