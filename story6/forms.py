from django import forms
from .models import Kegiatan, Pendaftar

class FormKegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = '__all__'

class FormPendaftar(forms.ModelForm):
    class Meta:
        model = Pendaftar
        fields = '__all__'