from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Kegiatan, Pendaftar
from .forms import FormKegiatan, FormPendaftar


def index(request):
    form = FormPendaftar(request.POST or None)
    kegiatan = Kegiatan.objects.all()
    context = {
        'daftar_kegiatan' : kegiatan,
        'form_pendaftar' : FormPendaftar,
    }

    return render(request,'story6/index.html', context)

def add_pendaftar(request, id=None):
    form_pendaftar = FormPendaftar(request.POST or None)
    kegiatan_pendaftar = Kegiatan.objects.get(id=id)

    if (form_pendaftar.is_valid() and request.method == 'POST'):
        nama_pendaftar = form_pendaftar["nama_pendaftar"].value()
        pendaftar = Pendaftar(kegiatan_pendaftar=kegiatan_pendaftar, nama_pendaftar=nama_pendaftar)
        pendaftar.save()
    
    return HttpResponseRedirect('/story6/')

def add_kegiatan(request):
    form_kegiatan = FormKegiatan(request.POST or None)
    context = {
        "form_kegiatan" : FormKegiatan,
    }

    if (form_kegiatan.is_valid() and request.method == 'POST'):
        form_kegiatan.save()
        return HttpResponseRedirect('story6:index')

    return render(request, 'story6/form-kegiatan.html', context)

# def index(request):
#     if (request.method == 'POST'):
#         kegiatan = Kegiatan.objects.all()
#         form_pendaftar = FormPendaftar(request.POST or None)
#         nama_pendaftar = form_pendaftar["nama_pendaftar"].value()
#         id_peserta = int(request.POST['id'])
#         kegiatan_pendaftar = Kegiatan.objects.get(id=id_peserta)
#         pendaftar = Pendaftar(kegiatan_pendaftar=kegiatan_pendaftar, nama_pendaftar=nama_pendaftar)
#         pendaftar.save()
#         daftar_peserta = Pendaftar.objects.all()

#         context = {
#             'daftar_kegiatan' : kegiatan,
#             'form_pendaftar' : FormPendaftar,
#             'daftar_peserta' : daftar_peserta,
#         }

#         return render(request, 'story6/index.html', context)

#     else:
#         kegiatan = Kegiatan.objects.all()
#         daftar_peserta = Pendaftar.objects.all()

#         context = {
#             'daftar_kegiatan' : kegiatan,
#             'daftar_peserta' : daftar_peserta,
#         }

#         return render(request, 'story6/index.html', context)
