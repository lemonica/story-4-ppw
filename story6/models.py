from django.db import models

class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=100)

    def __str__(self):
        return self.kegiatan

class Pendaftar(models.Model):
    nama_pendaftar = models.CharField(max_length=100)
    kegiatan_pendaftar = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, related_name='pendaftar')

    def __str__(self):
        return self.nama

