from django.urls import path
from . import views

app_name = "story6"

urlpatterns = [
    path('', views.index, name='index'),
    path('form-pendaftar/<int:id>', views.add_pendaftar, name='addPendaftar'),
    path('form-kegiatan/', views.add_kegiatan, name='addKegiatan'),
]
